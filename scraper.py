from bs4 import BeautifulSoup
from urllib.request import urlopen

url = "https://ca.indeed.com/jobs?q=%22data+engineer%22&l=Toronto%2C+ON"
page = urlopen(url)
html = page.read().decode("utf=8")
soup = BeautifulSoup(html, "html.parser")

# print(soup.get_text().replace("\n", ""))
for link in soup.find_all('a'):
    title = str(link.get('title')).lower()
    if 'data' in title and 'engineer' in title:
        print(link.get('href') + "\n")